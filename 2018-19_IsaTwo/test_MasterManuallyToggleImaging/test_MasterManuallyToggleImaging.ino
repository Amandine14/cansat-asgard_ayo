
#include "IsaTwoConfig.h"
#include "ClockSynchronizer.h"
#include "Wire.h"

void setup() {
  Serial.begin(115200);
  while (!Serial) ;
  Serial << ENDL;
  Serial << "=============================================" << ENDL;
  Serial << "=    Utility to manually toggle imager      =" << ENDL;
  Serial << "=============================================" << ENDL<<ENDL;
   Serial << "Configuring as output and setting to HIGH" << ENDL;

  Wire.begin();
  pinMode(ImagerCtrl_MasterDigitalPinNbr, OUTPUT);
  digitalWrite(ImagerCtrl_MasterDigitalPinNbr, HIGH);

  Serial << "Enter any character followed by ENTER to toggle the imager" << ENDL;
}

void toggleImager() {
  if (digitalRead(ImagerCtrl_MasterDigitalPinNbr) == LOW) {
    Serial << "Deactivating Imager..." << ENDL;
    digitalWrite(ImagerCtrl_MasterDigitalPinNbr, HIGH);
  } else {
    Serial << "Activating Imager..." << ENDL;
    digitalWrite(ImagerCtrl_MasterDigitalPinNbr, LOW);
    delay(2); // Allow imager to detect and wait for master clock.
    Serial << " Transmitting clock...";
    if (ClockSynchronizer_Master::setMasterClock(I2C_SlaveControllerAddress, 1)) {
      Serial << "OK!" << ENDL;
    }
    else {
      Serial << ENDL << " **** Error in transmission (cancelling) ***";
      digitalWrite(ImagerCtrl_MasterDigitalPinNbr, HIGH); // cancelling to avoid deadlock
    }
  }
}

void loop() {
  if (Serial.available()) {
    toggleImager();
    while (Serial.available()) {
      Serial.read();
    }
  }
}
