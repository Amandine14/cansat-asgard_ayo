/*
 * IsaTwoRecordGoodies
 */

 /**
 * @file This file gathers the implementation of all IsaTwoRecord methods which are not used in an
 * operational context. This avoid those methods to be linked in the operational software, hence 
 * saving memory.
 */

#include "IsaTwoRecord.h"
#define DEBUG
#include <DebugCSPU.h>
#define DBG 1

void IsaTwoRecord::print(Stream& str, const float arr[3]) const {
  str << '[';
  str.print(arr[0], numDecimalPositionsToUse); str << ',';
  str.print(arr[1], numDecimalPositionsToUse); str << ',';
  str.print(arr[2], numDecimalPositionsToUse);
  str << ']';
}

void IsaTwoRecord::print(Stream& str, const int16_t arr[3]) const {
  str << '[' << arr[0] << ',' << arr[1] << ',' << arr[2] << ']';
}

void IsaTwoRecord::print_Part(Stream& str, DataSelector select) const {
  switch (select) {
    case DataSelector::IMU :
      str << ENDL;
      str << (F("** IMU DATA **")) << ENDL;
      str << (F(" accelRaw     : ")); print(str, accelRaw); str << ENDL;
      str << (F(" gyroRaw      : ")); print(str, gyroRaw); str << ENDL;
      str << (F(" magnet. (µT) : ")); print(str, mag);
      break;
    case DataSelector::GPS_AHRS :
      str << ENDL;
      str << (F("** GPS DATA **")) << ENDL;
      str << (F(" GPS_Measures           : ")); printCSV(str, newGPS_Measures); str << ENDL;
      str << (F(" GPS_Latitude (deg)     : ")); printCSV(str, GPS_LatitudeDegrees); str << ENDL;
      str << (F(" GPS_Longitude(deg)     : ")); printCSV(str, GPS_LongitudeDegrees); str << ENDL;
      str << (F(" GPS_Altitude (m)       : ")); printCSV(str, GPS_Altitude); str << ENDL;
#ifdef INCLUDE_GPS_VELOCITY
      str << (F(" GPS_Velocity (knots)   : ")); printCSV(str, GPS_VelocityKnots); str << ENDL;
      str << (F(" GPS_VelocityAngle (deg): ")); printCSV(str, GPS_VelocityAngleDegrees); str << ENDL;
#endif
#ifdef INCLUDE_AHRS_DATA
      str << (F("** AHRS DATA **")) << ENDL;
      str << (F(" AHRS_Accel (m/s^2): ")); print(str, AHRS_Accel); str << ENDL;
      str << (F(" Roll  (rad)       : ")); printCSV(str, roll); str << ENDL;
      str << (F(" Yaw   (rad)       : ")); printCSV(str, yaw); str << ENDL;
      str << (F(" Pitch (rad)       : ")); printCSV(str, pitch); str << ENDL;
#endif
      break;
    case DataSelector::PrimarySecondary :
      str << (F("** Primary/secundary Mission **")) << ENDL;
      str << (F(" Temp.BMP (°C) : ")); printCSV(str, temperatureBMP); str << ENDL;
      str << (F(" Pressure (hPa): ")); printCSV(str, pressure); str << ENDL;
      str << (F(" Altitude (m)  : ")); printCSV(str, altitude); str << ENDL;
      str << (F(" Temp.Thermitor (°C): ")); printCSV(str, temperatureThermistor); str << ENDL;
      str << (F(" eCO2 (ppm)    : ")); printCSV(str, co2); str << ENDL;

      break;
    default:
      DPRINT(DBG,"Unexpected DataSelector value. Terminating Program");
      DASSERT(false);
  }
}
void IsaTwoRecord::print(Stream& str, DataSelector select) const {
  str << F("Record type = DataRecord, ts (msec) = ") << timestamp << ENDL;
  if (select == DataSelector::All) {
    print_Part(str, DataSelector::IMU);
    print_Part(str, DataSelector::GPS_AHRS);
    print_Part(str, DataSelector::PrimarySecondary);
  } else {
    print_Part(str, select);
  }
}
