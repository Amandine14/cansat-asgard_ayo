/*
    IsaTwoAcquisitionProcess.cpp

    2018.01.28: Possible memory optimisation to be evaluated: The HardwareScanner could be
    dynamically allocated and dispose of when init() completes. Benefit is not
    clear, unless memory consumption increases after init() (currently not confirmed).

*/

#include "IsaTwoAcquisitionProcess.h"
#include "HardwareScanner.h"
#include "IsaTwoConfig.h"
#include <Wire.h>
#include <SPI.h>
#include "IsaTwoRecord.h"
#include "StringStream.h"
#include "Timer.h"
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
#include "Serial2.h"
#endif

IsaTwoAcquisitionProcess::IsaTwoAcquisitionProcess()
  : AcquisitionProcess(	IsaTwoAcquisitionPeriod),
	hwScanner(unusedAnalogInPinNumber),
    imu(imuSamplesPerReading), bmp(), co2(),
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
	gps((GPS_SerialPortNumber==1 ? Serial1 : Serial2)),
#endif
	thermistor(Thermistor_AnalogInPinNbr),
	storageMgr(IsaTwoAcquisitionPeriod, IsaTwoCampainDuration),
	elapsedSinceSlaveSync(0L),
	averageSpeed(0.0f)
{
  campaignStarted = false;
  lastRecordAltitude = -1;
}

void IsaTwoAcquisitionProcess::init() {
  DPRINTSLN(DBG_INIT, "IsaTwoProcess::init");

  // Initialize scanner BEFORE calling AcquisitionProcess::init
  hwScanner.IsaTwoInit();
#ifdef PRINT_DIAGNOSTIC_AT_INIT
  hwScanner.printFullDiagnostic(Serial);
  //Serial << sizeof(hwScanner) << "," << sizeof(buffer) <<","<<sizeof(storageMgr) <<","<< sizeof(bmp) << ENDL;
  DDELAY(DBG_INIT, 1000);
#endif

  AcquisitionProcess::init();
  String CSV_Header;
  CSV_Header.reserve(200);
  StringStream sstr(CSV_Header);
  buffer.printCSV_Header(sstr);
  storageMgr.init(&hwScanner, "ISA2", MasterSD_CardChipSelect, SD_RequiredFreeMegs, EEPROM_KeyValue, CSV_Header);

  auto RF=GET_RF_STREAM(hwScanner);
  if (RF != NULL)
  {
	RF_OPEN_STRING(RF);
    *RF << F("\nLogging to ") << storageMgr.getSD_FileName() << ENDL;
    RF_CLOSE_STRING(RF);
  }
  
#if (defined(ISATWO_USE_EEPROMS) && (DBG_DIAGNOSTIC==1))
  float duration = ((float) storageMgr.getNumFreeEEEPROM_Records()) * IsaTwoAcquisitionPeriod / 1000.0 / 60.0;
  Serial << F("EEPROM can accept ") << duration << F(" min. of data. (key=0x");
  Serial.print(EEPROM_KeyValue, HEX);
  Serial << ")" << ENDL;
  if (RF != NULL)
  {
	RF_OPEN_STRING(RF);
    *RF << F("EEPROM can accept ") << duration << F(" min. of data. Key=0x");
    RF->println(EEPROM_KeyValue, HEX); // This is currently not supported in API mode. Method to be added.
    RF_CLOSE_STRING(RF);
  }
#endif

#ifdef PRINT_ACQUIRED_DATA_CSV
  {
    Serial << ENDL;
    buffer.printCSV_Header(Serial);
    Serial << ENDL;
  }
#endif

  DPRINTSLN(DBG_INIT, "IsaTwoProcess::init done");
}

IsaTwoHW_Scanner*  IsaTwoAcquisitionProcess::getHardwareScanner()
{
  return &hwScanner;
}

void IsaTwoAcquisitionProcess::startMeasurementCampaign(const char* msg, float value) {
	campaignStarted = true;
	setLED(HardwareScanner::Campaign, AcqOff);

	// Do not start imager: it will be started during the next run
	// of AcquireDataRecord, since the campaignStarted will be true,
	// and the control line HIGH.
	String str("# === CAMPAIGN STARTED at ");
		// Initial # to create a comment line in the SD file.
	StringStream ss(str);
	ss << millis() << ": "<< msg << " (" << value << ") ===";
	DPRINTLN(DBG_DIAGNOSTIC, str.c_str());
	auto RF=GET_RF_STREAM(hwScanner);
	if (RF != NULL) {
		RF_OPEN_STRING(RF);
		*RF << str.c_str() << ENDL;
		RF_CLOSE_STRING(RF);
	}
	storageMgr.storeString(str);
}

void IsaTwoAcquisitionProcess::stopMeasurementCampaign() {
	campaignStarted = false;
	lastRecordAltitude = -1;
	averageSpeed=0.0f;
	setLED(HardwareScanner::Campaign, AcqOn);

	// Stop the imager.
	digitalWrite(ImagerCtrl_MasterDigitalPinNbr,HIGH);

	DPRINTSLN(DBG_DIAGNOSTIC, "");
	DPRINTSLN(DBG_DIAGNOSTIC, "=== CAMPAIGN STOPPED ===");
	auto RF=GET_RF_STREAM(hwScanner);
	if (RF != NULL) {
		RF_OPEN_STRING(RF);
		*RF << ENDL << F("=== CAMPAIGN STOPPED ===") << ENDL;
		RF_CLOSE_STRING(RF);
	}
}
void IsaTwoAcquisitionProcess::startImager() {
	DPRINTS(DBG_DIAGNOSTIC, "Starting Imager....");
	digitalWrite(ImagerCtrl_MasterDigitalPinNbr, LOW);
	unsigned long activationClock=millis();
	String activationMsg("** Imager activated at ");
	activationMsg+= String(activationClock);
	activationMsg+= " ***";
	DPRINTLN(DBG_DIAGNOSTIC,activationMsg.c_str());
	auto RF=GET_RF_STREAM(hwScanner);
	if (RF != NULL) {
		RF_OPEN_STRING(RF);
		*RF << activationMsg << ENDL;
		RF_CLOSE_STRING(RF);
	}
	storageMgr.storeString(activationMsg);
}

bool IsaTwoAcquisitionProcess::measurementCampaignStarted() {
  static byte ignoredSamples=10;
  static elapsedMillis elapsedSinceLastInfo=0;
  static unsigned long lastRecordTimestamp=0;

  if (campaignStarted == true ) {
    return true;
  }

  // Let's not start before readings are stable
  if (ignoredSamples >0) {
	  ignoredSamples--;
	  lastRecordTimestamp= buffer.timestamp;  // maintain the timestamp, otherwise it would cause an interruption and a new reset.
	  return false;
  }

  // Check absolute altitude. This is a security, to cover for a µC reset during the flight
  if (buffer.altitude >= AltitudeThresholdToStartCampaign) {
	  DPRINTS(DBG_DIAGNOSTIC, "Forcing campaign start because altitude > ");
	  DPRINTLN(DBG_DIAGNOSTIC, AltitudeThresholdToStartCampaign);
	  startMeasurementCampaign("Altitude above threshold!", buffer.altitude);
	  return true;
  }

  // Acquisition mode could have been interrupted, and our last information
  // could be outdated. In this case, we start averaging again
  if ((buffer.timestamp - lastRecordTimestamp) > (3*IsaTwoAcquisitionPeriod)) {
	  DPRINTSLN(DBG_DIAGNOSTIC, "Reseting average speed calculation");
	  lastRecordTimestamp= buffer.timestamp;
	  lastRecordAltitude=-1; // This will cause a reset of the averaging.
	  ignoredSamples=10;     // This will cause the next 10 samples to be ignored.
	  return false;
  }

  if (lastRecordAltitude == -1) {
	averageSpeed=0.0f; // Reset here in case the campaign was stopped manually after startup
	lastRecordAltitude = buffer.altitude;
    lastRecordTimestamp= buffer.timestamp;

    DPRINTSLN(DBG_DIAGNOSTIC, "");
    DPRINTS(DBG_DIAGNOSTIC, " ...Waiting for average speed of ");
    DPRINT(DBG_DIAGNOSTIC, MinSpeedToStartCompaign);
    DPRINTS(DBG_DIAGNOSTIC, " m/s during ");
    DPRINT(DBG_DIAGNOSTIC, NumSamplesToAverageForStartingCampaign);
    DPRINTS(DBG_DIAGNOSTIC, " readings. last rec. altitude=");
    DPRINTLN(DBG_DIAGNOSTIC, lastRecordAltitude);
    return false;
  }



  float speed = (buffer.altitude - lastRecordAltitude) / (buffer.timestamp - lastRecordTimestamp) * 1000;
  averageSpeed -= averageSpeed / NumSamplesToAverageForStartingCampaign;
  averageSpeed += speed / NumSamplesToAverageForStartingCampaign;

#ifdef DBG_CAMPAIGN_STARTED
  if ((elapsedSinceLastInfo > ReportingPeriodWhileWaitingForStartCampaign) &&
		  (averageSpeed > 2.0*MinSpeedToStartCompaign/3.0) ){
	  auto RF=GET_RF_STREAM(hwScanner);
	  if (RF != NULL)
	  {
		  	 RF_OPEN_STRING(RF);
		  	 *RF << millis() << ": Waiting for take-off..." << ENDL;
		  	 RF_CLOSE_STRING(RF);
		  	 RF_OPEN_STRING(RF);
		  	 *RF <<  "   current  alt.= " << buffer.altitude << ", prev. alt.= " << lastRecordAltitude << ENDL;
		  	 RF_CLOSE_STRING(RF);
		  	 RF_OPEN_STRING(RF);
		  	 *RF <<  "   current speed= " << speed << ", avg speed= " << averageSpeed << ENDL;
		  	 RF_CLOSE_STRING(RF);
	  }
	  DPRINTS (DBG_CAMPAIGN_STARTED, "Waiting for take-off.  curr. alt.=");
	  DPRINT(DBG_CAMPAIGN_STARTED, buffer.altitude);
	  DPRINTS (DBG_CAMPAIGN_STARTED, "  prev. altitude=");
	  DPRINT(DBG_CAMPAIGN_STARTED, lastRecordAltitude);
	  DPRINTS (DBG_CAMPAIGN_STARTED, "  speed=");
	  DPRINT(DBG_CAMPAIGN_STARTED, speed);
	  DPRINTS (DBG_CAMPAIGN_STARTED, "  avg speed=");
	  DPRINTLN(DBG_CAMPAIGN_STARTED, averageSpeed);
	  elapsedSinceLastInfo=0;
  }
#endif
  lastRecordAltitude = buffer.altitude;
  lastRecordTimestamp= buffer.timestamp;

  if (averageSpeed > MinSpeedToStartCompaign) {
	startMeasurementCampaign("Average speed above threshold!", averageSpeed);
    return true;
  } else {
    return false;
  }
}

void IsaTwoAcquisitionProcess::acquireDataRecord() {
  static byte recordCounter = 0
  DBG_TIMER("IsaTwoAcqProc::acqDataRec");
  DPRINTSLN(DBG_ACQUIRE, "IsaTwoAcquisitionProcess::acquireDataRecord()");
  buffer.clear();
  buffer.timestamp = millis();

  // use clients to fill buffer.
  DPRINTSLN(DBG_ACQUIRE, "Reading IMU");
  imu.readData(buffer);
  DPRINTSLN(DBG_ACQUIRE, "Reading BMP");
  bmp.readData (buffer);
  DPRINTSLN(DBG_ACQUIRE, "Reading C02");
  co2.readData (buffer);
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
  DPRINTSLN(DBG_ACQUIRE, "Reading GPS");
  gps.readData(buffer);
#endif
  DPRINTSLN(DBG_ACQUIRE, "Reading Thermistor");
  thermistor.readData(buffer);


#ifdef PRINT_ACQUIRED_DATA
  {
    DBG_TIMER("Serial output");
    Serial << ENDL;
    buffer.print(Serial);
  }
#endif

#ifdef PRINT_ACQUIRED_DATA_CSV
  if (campaignStarted || (recordCounter >= 50))
  {
    DBG_TIMER("Serial output CSV");
    buffer.printCSV(Serial);
    Serial.println();
    if (!campaignStarted) {
  	  Serial.println( buffer.newGPS_Measures ? "GPS Fix OK" : "No GPS fix.");
    }
  }
#endif
  DDELAY(DBG_SLOW_DOWN_TRANSMISSION, 500);

  // send on RF
  auto RF=GET_RF_STREAM(hwScanner);
  if (RF != NULL)
  {
    DBG_TIMER("RF transmission");
    if (campaignStarted || (recordCounter >= 50)) {
      setLED(HardwareScanner::Transmission, AcqOn);
#ifdef RF_ACTIVATE_API_MODE
      RF->send(buffer);
#else
      buffer.printCSV(*RF); // Print slowly to avoid buffer overflow.
      RF->println();
#endif
      recordCounter = 0;
      if (!campaignStarted) {
    	  RF_OPEN_STRING(RF);
    	  *RF <<( buffer.newGPS_Measures ? "GPS Fix OK" : "No GPS fix.") << ENDL;
    	  RF_CLOSE_STRING(RF);
    	  String CSV_Buffer;
    	  StringStream str(CSV_Buffer);
    	  buffer.printCSV(str);
    	  RF_OPEN_STRING_PART(RF);
    	  *RF << CSV_Buffer.substring(0,80) << ENDL;
    	  RF_CLOSE_STRING_PART(RF);
    	  RF_OPEN_STRING(RF);
    	  *RF << CSV_Buffer.substring(81) << ENDL;
    	  RF_CLOSE_STRING(RF);
    	  DPRINTLN(DBG_DIAGNOSTIC, CSV_Buffer.c_str());
      }
      setLED(HardwareScanner::Transmission, AcqOff);
    }
  } // RF not null
  recordCounter++;  // Increase even if not printed or sent on RF.

  if (campaignStarted) {
	  if (digitalRead(ImagerCtrl_MasterDigitalPinNbr)==HIGH) {
		  // campaign started and Imager is not active: turn it on.
		  DPRINTSLN(DBG_SLAVE_SYNC,"Starting imager");
		  startImager();
	  }
  } // campaign started.
  DPRINTSLN(DBG_ACQUIRE, "End acquireDataRecord()");
}

void IsaTwoAcquisitionProcess::storeDataRecord(const bool campaignStarted) {
  if (campaignStarted) {
#ifdef ISATWO_USE_EEPROMS
    setLED(HardwareScanner::UsingEEPROM, AcqOn); // The storage LED is managed by IsaTwoAcquisitionProcess::run().
    storageMgr.storeIsaTwoRecord( buffer, true); // during campaign, store in EEPROM
    setLED(HardwareScanner::UsingEEPROM  , AcqOff);
#else
    storageMgr.storeIsaTwoRecord( buffer, false);
#endif
  } else {
    storageMgr.storeIsaTwoRecord( buffer, false); // Out of campaign do not store in EEPROM
  }
}

void IsaTwoAcquisitionProcess::initSpecificProject()  {
  DPRINTSLN(DBG_INIT, "IsaTwoProcess::initSpecificProject");
  bool result;

  if (hwScanner.isI2C_SlaveUpAndRunning(I2C_IMU_SensorAddress)) {
     result =  imu.begin();
     if (!result) {
    	 DPRINTSLN(DBG_DIAGNOSTIC, "Error initializing IMU");
     }
  }

  if (hwScanner.isI2C_SlaveUpAndRunning(I2C_BMP_SensorAddress)) {
     result = bmp.begin(SeaLevelPressure_HPa);
     if (!result) {
    	 DPRINTSLN(DBG_DIAGNOSTIC, "Error initializing BMP");
     }
  }

  result=co2.beginUsingAnalogWakePin(CO2_AnalogWakePinNbr);
  if (!result) {
     DPRINTSLN(DBG_DIAGNOSTIC, "Error initializing CO2 sensor");
  }
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
  gps.begin();
#endif

  thermistor.begin(hwScanner.getDefaultReferenceVoltage(), hwScanner.getNumADC_Steps());

  auto RF=GET_RF_STREAM(hwScanner);
  // Send diagnostic and header to RF
  if (RF != NULL)
  {
	  setLED(HardwareScanner::Transmission, AcqOn);
	  // This is ugly and highly inefficient: to be redesigned...
	  String stringBuffer;
	  StringStream s(stringBuffer);
	  hwScanner.printFullDiagnostic(s);
	  auto last=stringBuffer.length()-1;
	  unsigned int first=0;
	  while (first <= last) {

		  if (first + 80 < last) {
			  RF_OPEN_STRING_PART(RF);
			  *RF << stringBuffer.substring(first, first+80);
			  first+=80;
			  RF_CLOSE_STRING_PART(RF);
		  } else {
			  RF_OPEN_STRING(RF);
			  *RF << stringBuffer.substring(first, last).c_str();
			  first=last+1;
			  RF_CLOSE_STRING(RF);
		  }

		  delay(100);
	  }
	  setLED(HardwareScanner::Transmission, AcqOff);
  }
  digitalWrite(ImagerCtrl_MasterDigitalPinNbr,HIGH); // Do not start imager.
  buffer.clear();

  DPRINTS(DBG_DIAGNOSTIC, "IsaTwoSensorsWarmupDelay...");
  delay(IsaTwoSensorsWarmupDelay);
  DPRINTSLN(DBG_DIAGNOSTIC, "Done.");

  DPRINTSLN(DBG_INIT, "End of IsaTwoProcess::initSpecificProject");
}

void IsaTwoAcquisitionProcess::doIdle()
{
   storageMgr.doIdle();
}
