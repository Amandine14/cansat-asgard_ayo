/*
   AcquisitionProcess.cpp

   Created AcqOn: 19 janv. 2018
*/


#include "AcquisitionProcess.h"
#include "Arduino.h"
#define DEBUG
//#define USE_ASSERTION
//#define USE_TIMER
#include "DebugCSPU.h"
#include "Timer.h"
#define DBG_RUN 0

AcquisitionProcess::AcquisitionProcess(
  unsigned int acquisitionPeriodInMsec) {
  periodInMsec = acquisitionPeriodInMsec;
  elapsedTime = acquisitionPeriodInMsec + 1; // Ensure next run will trigger an acquisition cycle.
  heartbeatLED_State = AcqOff;
}

void AcquisitionProcess::blinkHeartbeatLED() {


  if ( (heartbeatTimer >= 1000) && (heartbeatLED_State == AcqOff) ) {
    setLED(HardwareScanner::Heartbeat, AcqOn);
    heartbeatTimer = 0 ;
    heartbeatLED_State = AcqOn;
  }

  if ((heartbeatTimer >= 1000) && (heartbeatLED_State == AcqOn) ) {
    setLED(HardwareScanner::Heartbeat, AcqOff);
    heartbeatTimer = 0 ;
    heartbeatLED_State = AcqOff;
  }
}

void AcquisitionProcess::run() {
  blinkHeartbeatLED();

  DASSERT(storageMgr != nullptr);
  if (elapsedTime >= periodInMsec) {
    DBG_TIMER("AcqProc::runReal");
    elapsedTime = 0;    // Reset timer here, to avoid adding processing time to the cycle duration.
    DPRINTSLN(DBG_RUN, "Running Process");
    setLED(HardwareScanner::Acquisition, AcqOn);
    acquireDataRecord();
    setLED(HardwareScanner::Acquisition, AcqOff);

    if (isRecordRelevant()) {
      setLED(HardwareScanner::Storage, AcqOn);
      storeDataRecord(measurementCampaignStarted());
      setLED(HardwareScanner::Storage, AcqOff);
    }
  } else doIdle();
}

void AcquisitionProcess::initLED_Hardware()  {
  HardwareScanner *hw = getHardwareScanner();
  byte pin;
  pin=hw->getLED_PinNbr(HardwareScanner::Init);
  if (pin) pinMode(pin, OUTPUT);
  pin=hw->getLED_PinNbr(HardwareScanner::Storage);
  if (pin) pinMode(pin, OUTPUT);
  pin=hw->getLED_PinNbr(HardwareScanner::Transmission);
  if (pin) pinMode(pin, OUTPUT);
  pin=hw->getLED_PinNbr(HardwareScanner::Acquisition);
  if (pin) pinMode(pin, OUTPUT);
  pin=hw->getLED_PinNbr(HardwareScanner::Heartbeat);
  if (pin) pinMode(pin, OUTPUT);
  pin=hw->getLED_PinNbr(HardwareScanner::Campaign);
  if (pin) pinMode(pin, OUTPUT);
  pin=hw->getLED_PinNbr(HardwareScanner::UsingEEPROM);
  if (pin) pinMode(pin, OUTPUT);

  setLED(HardwareScanner::Init, AcqOn);
  setLED(HardwareScanner::Storage, AcqOn);
  setLED(HardwareScanner::Transmission, AcqOn);
  setLED(HardwareScanner::Acquisition, AcqOn);
  setLED(HardwareScanner::Heartbeat, AcqOn);
  setLED(HardwareScanner::Campaign, AcqOn);
  setLED(HardwareScanner::UsingEEPROM, AcqOn);
  delay(1000);  // All LEDs on for 1000 ms.
  setLED(HardwareScanner::Init, AcqOff);
  setLED(HardwareScanner::Storage, AcqOff);
  setLED(HardwareScanner::Transmission, AcqOff);
  setLED(HardwareScanner::Acquisition, AcqOff);
  setLED(HardwareScanner::Heartbeat, AcqOff);
  setLED(HardwareScanner::Campaign, AcqOff);
  setLED(HardwareScanner::UsingEEPROM, AcqOff);
}

void  AcquisitionProcess::init() {
  initLED_Hardware();
  setLED(HardwareScanner::Init, AcqOn);
  setLED(HardwareScanner::Campaign, AcqOn); // This LED remains On Until campaign is started.
  initSpecificProject();
  setLED(HardwareScanner::Init, AcqOff);
}

void AcquisitionProcess::setLED(HardwareScanner::LED_Type type, bool status) {
  byte pinNumber = getHardwareScanner()->getLED_PinNbr(type);
  if (pinNumber==0) return;
#ifdef ARDUINO_TMinus
  digitalWrite(pinNumber, status ? LOW : HIGH);
  // write LOW sets LED ON on TMINUS.
#else
  digitalWrite(pinNumber, status ? HIGH : LOW);
#endif
}

bool AcquisitionProcess::isRecordRelevant() const {
  return true;
}

