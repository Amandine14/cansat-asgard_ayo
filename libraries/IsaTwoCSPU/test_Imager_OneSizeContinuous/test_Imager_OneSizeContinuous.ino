/*
   test_ImagerOneSizeContinuous

   Take pictures as fast as possible, in one size (select below). 
   Wiring: GND, VCC, SDL-SDA, CLK-MOSI-MISO, 2 chip select (SD) lines asdefined below. 
*/

#define DEBUG
#include "DebugCSPU.h"
#include <Wire.h>
#include <SPI.h>
#include "IsaTwoImager.h"
#include <SD.h>

const byte SD_CS = 9;
const byte SC_Camera = 10;

// Select size here (1 line)
uint8_t imgSize=OV2640_160x120; // (smallest)
// uint8_t imgSize=OV2640_176x144; 
// uint8_t imgSize=OV2640_320x240; 
// uint8_t imgSize=OV2640_352x288; 
// uint8_t imgSize=OV2640_640x480; 
// uint8_t imgSize=OV2640_800x600; 
// uint8_t imgSize=OV2640_1024x768; 
// uint8_t imgSize=OV2640_1280x1024
// uint8_t imgSize=OV2640_1600x1200; // (largest)

IsaTwoImager img(SC_Camera);
bool initOK;
unsigned int counter=1;

void  setup()
{
  DINIT(115200)
  SPI.begin();
  Wire.begin();

  Serial << F("Initialising the IsaTwoImager...") << ENDL;
  initOK = img.begin(SD_CS, SC_Camera);
  if (!initOK)
  {
    Serial << F("Error during IsaTwoImager.begin()") << ENDL;
  }
  img.setImageSize(imgSize);
  Serial << F("Image size set to ") << imgSize << ENDL;
  Serial << F("Taking pictures continuously");
}

void loop()
{
  if (!initOK) return; 
  counter++;
  Serial << counter << F(". ") ;
  img.image(millis());
}
